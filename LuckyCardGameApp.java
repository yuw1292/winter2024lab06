import java.util.Scanner;
public class LuckyCardGameApp {
	public static void main(String[] args) {
		
		Deck deck = new Deck();
		deck.shuffle();
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to the Lucky Card Game!!");
		System.out.println("How many cards do you wish to remove?");
		
		int numCardsToRemove = scanner.nextInt();
        while (numCardsToRemove < 0 || numCardsToRemove > 52) {
			System.out.println("please enter a number between 0 and 52");
			numCardsToRemove = scanner.nextInt();
        }
		
		System.out.println("The initial number of the deck is: " + deck.length());
		//remove number of cards
		for (int i =  0; i < numCardsToRemove; i++) {
			deck.drawTopCard();
		}
		System.out.println("Number of cards after removal: " + deck.length());
		
		deck.shuffle();
        System.out.println("Shuffled deck: ");
        System.out.println(deck);
	}
}
	